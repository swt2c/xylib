Source: xylib
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Stuart Prescott <stuart@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 libboost-dev,
 libbz2-dev,
 libwxgtk3.0-gtk3-dev,
 zlib1g-dev,
Standards-Version: 4.5.1
Section: science
Homepage: https://github.com/wojdyr/xylib
Vcs-Git: https://salsa.debian.org/science-team/xylib.git
Vcs-Browser: https://salsa.debian.org/science-team/xylib
Rules-Requires-Root: no

Package: libxy3v5
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 fityk,
Description: Library for reading x-y data from several file formats
 C++ library for reading files that contain x-y data from powder diffraction,
 spectroscopy and other experimental methods. The supported formats include:
 VAMAS, pdCIF, Bruker UXD and RAW, Philips UDF and RD, Rigaku DAT,
 Sietronics CPI, DBWS/DMPLOT, Koalariet XDD and others.

Package: libxy-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libxy3v5 (= ${binary:Version}),
 ${misc:Depends},
Description: xylib development files
 Development files for xylib.
 .
 xylib is a C++ library for reading files that contain x-y data from powder
 diffraction, spectroscopy and other experimental methods.

Package: libxy-bin
Architecture: any
Depends:
 libxy3v5 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: xylib - utilities
 xylib is a C++ library for reading files that contain x-y data from powder
 diffraction, spectroscopy and other experimental methods.
 .
 This package contains a small program xyconv that converts files supported
 by the xylib library to TSV (tab-separated values).
